﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimplifiedEyeTracker;
using System.Diagnostics;
using System.Windows.Threading;
using System.Timers;

namespace Emptydll
{
    public class calculate
    {
        public EyeTracker eyeTracker;
        public double primaryScreenWidth = 1024;
        public double primaryScreenHeight = 768;
        public string devicename = "none";
        public double gazex=0;
        public double gazey=0;
        public string informationlog;
        private static System.Timers.Timer aTimer = new System.Timers.Timer(2000);

        public double[,] arrxy=new double[2,100];
        public int nindex = 0;
        public double[,] outarrxy= new double[2, 100];
        public calculate(int iwidth = 1024, int iheight = 768)
        {
            primaryScreenWidth = iwidth;
            primaryScreenHeight = iheight;

            this.eyeTracker = new EyeTracker(iwidth, iheight);
            //this.devicename=this.eyeTracker.GetDeviceName();
            //Debug.Print($"Model: {this.eyeTracker.GetModel()}");
            //Debug.Print($"SerialNumber: {this.eyeTracker.GetSerialNumber()}");
            this.eyeTracker.OnGazeData += this.OnGazeData;
            this.eyeTracker.StartReceivingGazeData();

        }
        
        public double GetEyePositionX()
        {
            return gazex;
        }
        public double GetEyePositionY()
        {
            return gazey;
        }
        public string GetDeviceName()
        {
            this.devicename = this.eyeTracker.GetDeviceName();
            return devicename;
        }
        public double[,] GetGazeArray()
        {
            return outarrxy;
        }
        public void GetDataTimer(int _interval=200)
        {
            aTimer = new System.Timers.Timer(_interval);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }
        public int Getnindex()
        {
            return nindex;
        }
        public string Getinformationlog()
        {
            return informationlog;
        }
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            // Console.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff},x:{1:f}, y:{2:f}",e.SignalTime, this.gazex, this.gazex);            
            //Console.WriteLine(this.informationlog);
            Console.WriteLine("{0:HH:mm:ss.fff}", e.SignalTime);
            Array.Copy(arrxy, outarrxy, arrxy.Length);
            Console.WriteLine("{0}",outarrxy);

            nindex = 0;
            Array.Clear(arrxy, 0, arrxy.Length);
        }
        private void OnGazeData(object sender, SimplifiedGazeDataEventArgs e)
        {
            Dispatcher.CurrentDispatcher.Invoke(() =>
            {
                //gazex += 1;
                if (e.IsLeftValid)
                {
                    informationlog = "valid 1";
                }
                else
                {
                    informationlog = "invalid 1";
                }

                if (e.IsLeftValid && e.LeftX >= 0.0 && e.LeftY >= 0.0)//&& e.LeftX <= this.primaryScreenWidth && e.LeftY <= this.primaryScreenHeight
                {
                    Debug.Print($"DeviceTimeStamp: {e.DeviceTimeStamp}, SystemTimeStamp: {e.SystemTimeStamp}");
                    Debug.Print($"X: {e.LeftX}, Y: {e.LeftY}");
                    gazex = e.LeftX;
                    gazey = e.LeftY;
                    informationlog = "valid";

                    if (nindex <= 99)
                    {
                        arrxy[0, nindex] = e.LeftX;
                        arrxy[1, nindex] = e.LeftY;
                        nindex += 1;
                    }                    
                }
                else
                {
                    gazex = -1;
                    gazey = -1;
                    informationlog = "invalid";
                }
                
            });
        }
        public int Add (int a, int b)
        {
            return a + b;
        }
        public int Sub(int a, int b)
        {
            return a - b;
        }
    }
}
