﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using SimplifiedEyeTracker;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// private readonly EyeTracker eyeTracker;
    
    public partial class MainWindow : Window
    {
        private readonly EyeTracker eyeTracker;
        private readonly Ellipse gazeCircleShape;
        private readonly double primaryScreenWidth;
        private readonly double primaryScreenHeight;
        public MainWindow()
        {
            InitializeComponent();
            this.primaryScreenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            this.primaryScreenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;

            this.gazeCircleShape = new Ellipse()
            {
                Fill = Brushes.Gray,
                Width = 8.0,
                Height = 8.0
            };
            mainCanvas.Children.Add(this.gazeCircleShape);

            try
            {
                this.eyeTracker = new EyeTracker(System.Windows.SystemParameters.PrimaryScreenWidth, System.Windows.SystemParameters.PrimaryScreenHeight);
                Debug.Print($"Model: {this.eyeTracker.GetModel()}");
                Debug.Print($"SerialNumber: {this.eyeTracker.GetSerialNumber()}");
                this.eyeTracker.OnGazeData += this.OnGazeData;
                this.eyeTracker.StartReceivingGazeData();
            }
            catch (InvalidOperationException e)
            {
                if (MessageBox.Show(e.Message, "OK", MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    this.Close();
                }
            }
        }
        private void OnGazeData(object sender, SimplifiedGazeDataEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
            if (e.IsLeftValid && e.LeftX >= 0.0 && e.LeftX <= this.primaryScreenWidth && e.LeftY >= 0.0 && e.LeftY <= this.primaryScreenHeight)
                {
                Debug.Print($"DeviceTimeStamp: {e.DeviceTimeStamp}, SystemTimeStamp: {e.SystemTimeStamp}");
                Debug.Print($"X: {e.LeftX}, Y: {e.LeftY}");
                if (mainCanvas.Children.Contains(this.gazeCircleShape))
                {
                    Canvas.SetLeft(this.gazeCircleShape, e.LeftX);
                    Canvas.SetTop(this.gazeCircleShape, e.LeftY);
                }
            }
        });
        }
}
}
