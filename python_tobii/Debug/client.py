import socket

def Data():
    hostname, sld, tld, port = 'www', 'integralist', 'co.uk', 80
    target = '{}.{}.{}'.format(hostname, sld, tld)

    # create an ipv4 (AF_INET) socket object using the tcp protocol (SOCK_STREAM)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # connect the client
    # client.connect((target, port))
    client.connect(('127.0.0.1', 8000))

    # send some data (in this case a HTTP GET request)
    message='hello server'
    client.sendall(str.encode(message))
    #client.send()

    # receive the response data (4096 is recommended buffer size)
    response = client.recv(4096)

    print(response.decode("utf-8"))

    #print(type(response.decode("utf-8")))
    return response.decode("utf-8")

if __name__ == '__main__':
    Data()