# call "emptydll.dll" from console

The aim of this program is to test wether the "emptydll.dll" successfully called the eyetracker.

## Intro of the dlls

1) Tobii dll: Tobii.Research.dll, tobii_firmware_upgrade.dll,tobii_pro.dll

These dlls can not be imported into labview, but are needed when running the eyetracker.


2) intermedia dll: SimplifiedEyeTracker.dll, Emptydll.dll

SimplifiedEyeTracker.dll basically has everything a labview need to create an eyetracker and get eyeposition.

Emptydll.dll is a simplified version which did two things: a) create an eyetracker, b) subscribe the eyedata. This is the dll file that needs to be inported into the labview.

3) testing program: CallDll.exe

This is a program that testing if Emptydll.dll works. If 


## How to run the CallDll.exe

1) open a cmd window

2) navigate to this path

3) type "calldll.exe", then click enter

4) the name of the eyetracker device should be in the window. Eye data should be updated every 10 milliseconds.